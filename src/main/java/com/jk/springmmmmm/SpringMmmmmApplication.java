package com.jk.springmmmmm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMmmmmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMmmmmApplication.class, args);
    }

}

